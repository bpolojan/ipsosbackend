﻿using BackEnd.Requests;
using BackEnd.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class EmailController : ControllerBase
    {
        private IEmailSender _emailService; 
        public EmailController(IEmailSender emailService)
        {
            _emailService = emailService;
        }

        [HttpPost("api/v1/send")]    
        public async Task<IActionResult> SendEmail([FromBody] EmailRequest requestPost)
        {
            if (_emailService.SendEmail(requestPost.EmailAddresses, requestPost.Message, requestPost.Subject))
                return Ok();
            else
                return StatusCode(418);           
        }
    }
}
