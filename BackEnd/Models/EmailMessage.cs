﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd.Models
{
    public class EmailMessage
    {
        public EmailMessage()
        {
            To = new List<string>();
        }

        public List<string> To { get; set; }
        public string Subject { get; set; }
        public string Body { get; set; }
    }
}
