﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd.Requests
{
    public class EmailRequest
    {
        public List<string> EmailAddresses { get; set; }
        public string Subject { get; set; }
        public string Message { get; set; }
    }
}
