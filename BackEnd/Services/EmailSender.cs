﻿using BackEnd.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace BackEnd.Services
{
    public class EmailSender:IEmailSender
    {
        private IEmailProvider _emailProvider;

        public EmailSender(IEmailProvider emailProvider)
        {
            _emailProvider = emailProvider;
        }

        public bool SendEmail(List<string> emailAddresses, string message, string subject) 
        {
            if (!emailAddresses.Any(x => x.Contains("test")))
            {
                var emailMessage = new EmailMessage();
                emailMessage.To = emailAddresses;
                emailMessage.Body = message;
                emailMessage.Subject = subject;

                var emailSendResult = _emailProvider.SendEmail(emailMessage);
                return emailSendResult;
            }
            else
            {
                return false;
            }
        }
    }
}
